public with sharing class AccountController {
    public static List<Account> getAllActiveAccounts() {
        return [SELECT ID,Name,Active__c FROM Account Where Active__c='Yes' Limit 11];
    }
}